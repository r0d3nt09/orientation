# Docker
#### What is Docker?

Docker is a program for developers to develop, and run applications with containers. Docker is used to deploy our products in various envuronments.

![alt text](https://www.oreilly.com/library/view/learn-openshift/9781788992329/assets/7310b2e5-4a3d-4ee4-ad85-ee534de55540.png)

#### Docker Installation
<ol><li>Installing CE(Community Docker Engine)</li></ol>
<pre><code>$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable nightly test"
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io

// Check if docker is successfully installed in your system
$ sudo docker run hello-world
</code></pre>

![alt text](https://raw.githubusercontent.com/sangam14/dockercheatsheets/master/dockercheatsheet1.png)
