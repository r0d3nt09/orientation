# Git
Git is a free and open source distributed version-control system for tracking changes in source code during software development.

![alt text](https://www.edureka.co/blog/wp-content/uploads/2016/11/Git-Commit-Workflow-Git-Tutorial-10-Edureka.png)


#### Git Internals
Git has three main states that your files can reside in: modified, staged, and committed :
<ol>
<li>Modified means that you have changed the file but have not committed it to your repo yet.</li>
<li>Staged means that you have marked a modified file in its current version to go into your next picture/snapshot.</li>
<li>Committed means that the data is safely stored in your local repo in form of pictures/snapshots.</li>
</ol>

At one time there are 3/4 different trees of your software code/repository are present.
<ol>
<li>Workspace : All the changes you make via Editor(s) (gedit, notepad, vim, nano) is done in this tree of repository.</li>
<li>Staging : All the staged files go into this tree of your repository.</li>
<li>Local Repository : All the committed files go to this tree of your repository.</li>
<li>Remote Repository : This is the copy of your Local Repository but is stored in some server on the Internet. All the changes you commit into the Local Repository are not directly reflected into this tree. You need to push your changes to the Remote Repository.</li>

#### Getting Started and Required Commands

![alt text](https://res.cloudinary.com/practicaldev/image/fetch/s--ShHSfi-a--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://cl.ly/1N2U2i2Z2C16/Image%25202018-04-11%2520at%252012.47.23%2520PM.png)

#### Git Workflow

1. Clone the repo
<pre><code>$git clone (link-to-repo)</code></pre>
2. Create a new branch
<pre><code>$ git checkout master</code></pre>
<pre><code>$ git checkout -b (your-branch-name)</code></pre>
3. Modify files in the working tree.

4. Selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.
<pre><code>$ git add .         # To add untracked files ( . adds all files) </code></pre>
5. Do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Local Git Repository.
<pre><code>$ git commit -sv   # Description about the commit</code></pre>
6. Do a push, which takes the files as they are in the Local Git Repository and stores that snapshot permanently to your Remote Git Repository.
<pre><code>$ git push origin <branch-name>      # push changes into repository</code></pre>
